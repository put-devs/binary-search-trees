const { displayTree, listTree } = require('tree');
const tree = require('tree');
const prompt = require('prompt-sync')();



/////////          -- ZMIENNE--          /////////
let menuOption = '0';   // wybrana w menu opcja
let menuStyle = 0;  // styl wyświetlania menu 0-normalny, 1-błąd wprowadzenia
let userData = [];      // dane, na których pracujemy
let tree1 = [];
let tree2 = [];

let settings = {
  displayStyle: 2
};

/////////          -- FUNKCJE MENU --          /////////
function showData(){
  if (userData.length < 11) console.log(`| Dane: [${userData}]`);
  else console.log(`| Dane: [... ${userData.length} elementów ...]`);
  if (tree1.length){
    tree.displayTree(tree1,-1,'| Drzewo AVL: ');
    tree.displayTree(tree2,-1,'| Drzewo \'losowe\': ');
  }
}
function drawMainMenu(style){
  console.clear();
  console.log('-- MENU --');
  showData();
  console.log('Wprowadź numer operacji i zatwierź przyciskiem ENTER:');
  console.log(' 1. Wprowadź dane z klawiatury');
  console.log(' 2. Pobierz dane z generatora');
  console.log(' 3. Stwórz drzewa');
  console.log(' 4. Operacje na drzewach');
  console.log(' 5. Ustawienia');
  console.log(' 6. Testy');
  console.log(' 0. Wyjdź');
  if (style > 0) console.log('Podaj prawidłowy numer operacji!');
}
function drawTreeMenu(style){
  console.clear();
  console.log('-- OPERACJE NA DRZEWACH --');
  showData();
  console.log('Wprowadź numer operacji i zatwierź przyciskiem ENTER:');
  console.log(' 1. Wyszukaj minimum i maksimum');
  console.log(' 2. Usuń elementy');
  console.log(' 3. Wypisz elementy w wybranym porządku');
  console.log(' 4. Usuń wszystkie elementy z drzewa (w porządku post-order)');
  console.log(' 5. Zrównoważ drzewa przez usunięcie korzeni');
  console.log(' 6. Wyświetl drzewa');
  console.log(' 7. Wyświetl współczynniki równowagi');
  console.log(' 0. Cofnij');
  if (style > 0) console.log('Podaj prawidłowy numer operacji!');
}
function optionOne(){
  let option = '0';
  let style = 0;
  while(true){
    ////// TEKST
    console.clear();
    console.log('-- WPROWADZANIE DANYCH --');
    showData()
    console.log(` 1. Wprowadź dane`);
    console.log(` 2. Usuń ostatni element`);
    console.log(` 3. Wyczyść`);
    console.log(` 0. Powrót`);
    if (style > 0 ) console.log(`Podaj prawidłowy numer operacji!`);
    ////// INTERAKCJA
    style = 0;
    option = prompt();
    if (option === '1'){
      while(true){
        console.clear();
        console.log('-- WPROWADZANIE DANYCH --');
        showData()
        console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
        console.log(`Podaj element:`);
        if (style === 1 ) console.log(`Podaj prawidłowy element!`);
        if (style === 2 ) console.log(`Dane z klawiatury można wprowadzać dla liczby elementów nie większej od 10`);
        style = 0;
        option = prompt();
        if (option === '') break;
        if (/[a-z]/i.test(option)) {
          style = 1; continue;
        }
        option = parseInt(option, 10);
        if (isNaN(option)) {
          style = 1;
          continue;
        }
        if (userData.length > 9){
          style = 2;
        }
        else userData.push(option);
      }
      continue;
    }
    if (option === '2'){
      userData.pop();
      continue;
    }
    if (option === '3'){
      userData = [];
      continue;
    }
    if (option === '0') return;
    style = 1;
  }
}
function optionTwo(){
  let option = '0';
  let style = 0;
  while(true){
    console.clear();
    console.log('-- GENEROWANIE DANYCH --');
    console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
    console.log(`Podaj ilość elementów do wygenerowania (n):`);
    if (style > 0 ) console.log(`Podaj prawidłową wartość!`);
    style = 0;
    option = prompt();
    if (option === '') return;
    if (/[a-z]/i.test(option)) {
      style = 1; continue;
    }
    option = parseInt(option, 10);
    if (isNaN(option)) {
      style = 1;
      continue;
    }
    userData = tree.genData(option);
    return;
  }
}
function optionThree(){
  if (userData.length){
    console.clear();
    let treeTimer = performance.now();
    tree1 = tree.createAVL(userData);
    let treeAfterTimer = performance.now();
    let t1 = Math.floor((treeAfterTimer - treeTimer)*1000)/1000;
    console.log(`Tworzenie drzewa AVL trwało ${t1}ms`);
    treeTimer = performance.now();
    tree2 = tree.createRandom(userData);
    treeAfterTimer = performance.now();
    let t2 = Math.floor((treeAfterTimer - treeTimer)*1000)/1000;
    console.log(`Tworzenie drzewa 'losowego' trwało ${t2}ms`);
    prompt();
    return;
  }
  console.clear();
  console.log('Aby utworzyć drzewo proszę najpierw wprowadzić dane!');
  console.log('Wciśnij ENTER, aby kontynuować...');
  prompt();
}
function optionFour(){
  if (! tree1.length){
    console.clear();
    console.log('Aby wykonywać operacje na drzewach proszę najpierw je utworzyć!');
    console.log('Wciśnij ENTER, aby kontynuować...');
    prompt();
    return;
  }
  let treeMenuStyle = 0;
  while(true){
    drawTreeMenu(treeMenuStyle);
    treeMenuStyle = 0; //zakładamy, że nie zostanie popełniony błąd
    treeOption = prompt();
    if (treeOption === '1'){
      treeOne();
      continue;
    }
    if (treeOption === '2'){
      treeTwo();
      continue;
    }
    if (treeOption === '3'){
      treeThree();
      continue;
    }
    if (treeOption === '4'){
      treeFour();
      continue;
    }
    if (treeOption === '5'){
      treeFive();
      continue;
    }
    if (treeOption === '6'){
      treeSix();
      continue;
    }
    if (treeOption === '7'){
      treeSeven();
      continue;
    }
    if (treeOption === '0') {
      return;
    }
    treeMenuStyle = 1;
  }
}

function optionFive(){
  let option = '0';
  let style = 0;
  while(true){
    console.clear();
    console.log('-- USTAWIENIA --');
    console.log(`1. Styl wyświetlania drzew: ${settings.displayStyle}`);
    console.log('___________________');
    tree.displayTree([7,4,6,2,5,8], settings.displayStyle);
    console.log('___________________\n');
    if (style > 0 ) console.log(`Podaj prawidłową wartość!`);
    console.log(`0. Cofnij`);
    style = 0;
    option = prompt();
    if (option === '0') return;
    if (/[a-z]/i.test(option)) {
      style = 1; continue;
    }
    if (option === '1'){
      settings.displayStyle = (settings.displayStyle === 5)? 0 : settings.displayStyle+1;
      continue;
    }
    style = 1;
  }
}
function measure(fun, args){
  let t1 = performance.now();
  const out = fun(...args);
  let t2 = performance.now();
  let time = Math.floor((t2 - t1)*1000)/1000;
  return {out: out, time: time}
}
function optionSix(){
  console.clear();
  console.log('-- TESTOWANIE TWORZENIA DRZEW --');
  /*
  Wygeneruj n-elementowe posortowane malejąco ciągi liczb naturalnych (dla 10-15 różnych wartości n z przedziału <10,k>,
    przy czym k należy dobrać eksperymentalnie tak, aby możliwe było wykonanie pomiarów i aby jego wartość była możliwie duża).
  */
  let ns = [];
  let currentValue = 50;//5000
  let krok = 100;//10000
  for (let x = 0; x<10; ++x){
    ns.push(currentValue);
    currentValue+=krok;
  }
  let NUMBER = 10;
  let min = -1000000000;
  let max = 1000000000;
  let testData = [];
  let avls = [];
  let bsts = [];
  let debug = false;
  testData = [];
  for(let x=0;x<NUMBER;++x){
    ns.forEach((n) => {
      testData.push(tree.genData(n,max, min));//.sort((a,b) => a-b));
    });
  }
  testData.forEach((test) => {
    let sortedTest = [].concat(test);
    sortedTest.sort((a,b) => b-a);
    let {out: avl, time: avlTime} = measure(tree.createAVL, [sortedTest, false]);
    let {out: bst, time: bstTime} = measure(tree.createRandom, [test]);
    avls.push(avl);
    bsts.push(bst);
    //console.log(`Ilość elementów: ${test.length}`)
    //console.log(`Tworzenie drzewa AVL zajęło: ${avlTime}ms`)
    //console.log(`Tworzenie zwykłego drzewa BST zajęło: ${bstTime}ms\n`)
    console.log(`${test.length};${avlTime};${bstTime}`);
    if (debug){
      console.log(`AVL: ${avl.length}`);
      console.log(`BST: ${bst.length}\n`);
    }
  });
  console.log('Wciśnij ENTER, aby kontynuować...');
  prompt();
  console.clear();
  console.log('-- TESTOWANIE POSZUKIWANIA NAJMNIEJSZEGO ELEMENTU --');
  for(let i=0; i<avls.length;++i){
    let {time: avlTime} = measure(tree.getMin, [avls[i]]);
    let {time: bstTime} = measure(tree.getMin, [bsts[i]]);
    console.log(`${testData[i%NUMBER].length};${avlTime};${bstTime}`);
  }
  console.log('Wciśnij ENTER, aby kontynuować...');
  prompt();
  console.clear();
  console.log('-- TESTOWANIE USTAWIANIA ELEMENTÓW W KOLEJNOŚCI IN-ORDER --');
  for(let i=0; i<avls.length;++i){
    let {time: avlTime} = measure(tree.listTree, [avls[i], 1]);
    let {time: bstTime} = measure(tree.listTree, [bsts[i], 1]);
    console.log(`${testData[i%NUMBER].length};${avlTime};${bstTime}`);
  }
  console.log('Wciśnij ENTER, aby kontynuować...');
  prompt();
  console.clear();
  console.log('-- TESTOWANIE RÓWNOWAŻENIA DRZEWA BST --');
  for(let i=0; i<avls.length;++i){
    let {time: bstTime} = measure(tree.balance, [bsts[i]]);
    console.log(`${testData[i%NUMBER].length};${bstTime}`);
  }
  console.log('Wciśnij ENTER, aby zakończyć...');
  prompt();
}

/*
treeOne
  console.log('1. Wyszukaj minimum i maksimum');
  console.log('2. Usuń elementy');
  console.log('5. Usuń wszystkie elementy z drzewa');*/
function treeOne(){
  console.clear();
  let {el: min1, path: minPath1} = tree.getMin(tree1);
  let {el: max1, path: maxPath1} = tree.getMax(tree1);
  let {el: min2, path: minPath2} = tree.getMin(tree2);
  let {el: max2, path: maxPath2} = tree.getMax(tree2);
  if (! minPath1.length) minPath1 = '-';
  if (! maxPath1.length) maxPath1 = '-';
  if (! minPath2.length) minPath2 = '-';
  if (! maxPath2.length) maxPath2 = '-';
  console.log(`Drzewo AVL:\n  Minimum: ${min1}\n  Ścieżka: ${minPath1}\n  Maksimum: ${max1}\n  Ścieżka: ${maxPath1}`);
  console.log(`Drzewo \'losowe\':\n  Minimum: ${min2}\n  Ścieżka: ${minPath2}\n  Maksimum: ${max2}\n  Ścieżka: ${maxPath2}`);
  console.log('Wciśnij ENTER, aby kontynuować');
  prompt();
}








function treeTwo(){
  let option = '0';
  let style = 0;
  let toBeDeleted = 0;
  let toDelete = [];
  while(true){
    console.clear();
    console.log('-- USUWANIE ELEMENTÓW --');
    showData();
    console.log(`Nie wpisuj nic, aby powrócić do poprzedniego menu`);
    console.log(`Podaj ilość węzłów do usunięcia:`);
    if (style === 1 ) console.log(`Podaj prawidłową wartość!`);
    else if (style === 2 ) console.log(`Ilość węzłów musi wynosić co najmniej 1!`);
    else if (style === 3 ) console.log(`Ilość węzłów do usunięcia nie może być większa niż liczba elementów!`);
    
    style = 0;
    option = prompt();

    if (option === '') return;
    if (/[a-z]/i.test(option)) {
      style = 1;
      continue;
    }
    option = parseInt(option, 10);
    if (isNaN(option)) {
      style = 1;
      continue;
    }
    if (option < 1) {
      style = 2;
      continue;
    }
    if (option > userData.length) {
      style = 3;
      continue;
    }
    // PODANO POPRAWNĄ ILOŚĆ WĘZŁÓW DO USUNIĘCIA
    toBeDeleted = option;
      while(toDelete.length - toBeDeleted){
        console.clear();
        console.log('-- USUWANIE ELEMENTÓW --');
        showData();
        console.log(`Nie wpisuj nic, aby powrócić do poprzedniego wyboru. Wpisz 'undo', aby usunąć ostatni element spośród tych do usunięcia.`);
        console.log(`Elementy do usunięcia (${toDelete.length}/${toBeDeleted}): [${toDelete}]`);
        console.log(`Podaj element do usunięcia:`);
        if (style === 1 ) console.log(`Podaj prawidłową wartość!`);
        else if (style === 2 ) console.log(`Podana wartość nie występuje w drzewie!`);
        else if (style === 3 ) console.log(`Ta wartość już jest zaplanowana to usunięcia!`);

        style = 0;
        option = prompt();
        if (option === '') break;
        if (option === 'undo') {
          toDelete.pop();
          continue;
        }
        if (/[a-z]/i.test(option)) {
          style = 1;
          continue;
        }
        option = parseInt(option, 10);
        if (isNaN(option)) {
          style = 1;
          continue;
        }
        if (toDelete.indexOf(option) !== -1){
          style = 3;
          continue;
        }
        if (tree1.indexOf(option) === -1){
          style = 2;
          continue;
        }
        toDelete.push(option);
      }
      // PODANO ELEMENTY DO USUNIĘCIA
      toDelete.forEach((element) => {
        tree.removeElement(tree1, element);
        tree.removeElement(tree2, element);
      });
      return;
  }
}














let methods = ['pre-order', 'in-order', 'post-order'];
function treeThree(){
  //3.Wypisywanie elementów w wybranym porządku
  let option = '0';
  let style = 0;
  let stage = 0;
  let key;
  while(true){
    console.clear();
    console.log('-- WYPISYWANIE ELEMENTÓW --');
    if (userData.length < 11) console.log(`| Dane: [${userData}]`);
    else console.log(`| Dane: [... ${userData.length} elementów ...]`);
    console.log(`Nie wpisuj nic, aby wybrać opcję domyślną określoną w nawiasach. Wpisz 'exit', aby wyjść z tego menu`);
    if (stage === 0) console.log(`Podaj klucz [korzeń]:`);
    else {
      console.log(`Każdy porządek wyświetlania elementów ma przydzielony numer. Wybierz jeden z nich:`);
      console.log(`   [1] pre-order`);
      console.log(`   [2] in-order`);
      console.log(`   [3] post-order`);
      console.log(`Podaj numer [1]:`);
    }
    if (style === 1 ) console.log(`Podaj prawidłową wartość!`);
    //else if (style === 2 ) console.log(`Podana wartość nie występuje w drzewie!`);
    style = 0;
    option = prompt();
    if (option === 'exit') return;
    if (option === '') {
      if (stage === 0) option = 'root';
      else option = '1';
    }
    else {
      if (/[a-z]/i.test(option)) {
        style = 1;
        continue;
      }
      option = parseInt(option, 10);
      if (isNaN(option)) {
        style = 1;
        continue;
      }
      /*
      if (tree1.indexOf(option) === -1){
        style = 2;
        continue;
      }
      */
    }
    if (stage === 0){
      key = option;
      stage = 1;
      continue;
    }
    if (option > 3 || option < 1) {
      style = 1; continue;
    }
    let method = option-1; 
    console.clear();
    console.log(`Wyświetlanie drzew w porządku ${methods[method]}`);
    let t1 = performance.now();
    let avl = tree.listTree(tree1, method, key);
    let t2 = performance.now();
    let time1 = Math.floor((t2 - t1)*1000)/1000;
    let t3 = performance.now();
    let rand = tree.listTree(tree2, method, key)
    let t4 = performance.now();
    let time2 = Math.floor((t4 - t3)*1000)/1000;
    console.log(`| Drzewo AVL: ${avl}`);
    console.log(`Ustawianie elementów drzewa AVL w tej kolejności trwało ${time1}ms`);
    console.log(`__________\n| Drzewo \'losowe\': ${rand}`);
    console.log(`Ustawianie elementów drzewa 'losowego' w tej kolejności trwało ${time2}ms`);
    prompt();
    return;
  }
}
function treeFour(){
  let orderAVL = tree.listTree(tree1, 2);
  let orderRAND = tree.listTree(tree2, 2);
  console.log(`Usuwanie drzewa AVL:`);
  tree.displayTree(tree1,settings.displayStyle);
  console.log(`=============================\n`);
  orderAVL.forEach((element, index) => {
    console.log(`Usuwanie elementu ${element} (${index+1}/${orderAVL.length})`);
    tree.removeElement(tree1, element);
    if (index+1 < orderAVL.length){
      tree.displayTree(tree1,settings.displayStyle);
      console.log(`__________________________________________`);
    }
    else console.log(`\nDrzewo jest puste!\n\n`);
  });
  console.log(`Usuwanie drzewa 'losowego':`);
  tree.displayTree(tree2,settings.displayStyle);
  console.log(`=============================\n`);
  orderRAND.forEach((element, index) => {
    console.log(`Usuwanie elementu ${element} (${index+1}/${orderRAND.length})`);
    tree.removeElement(tree2, element);
    if (index+1 < orderRAND.length){
      tree.displayTree(tree2,settings.displayStyle);
      console.log(`__________________________________________`);
    }
    else console.log(`\nDrzewo jest puste!`);
  });
  prompt();
}
function treeFive(){
  console.clear();

  console.log('| Równoważenie drzewa AVL:');
  tree.displayTree(tree1,settings.displayStyle);
  tree.balance(tree1);
  prompt();

  console.log('\n| Drzewo po zrównoważeniu:');
  tree.displayTree(tree1,settings.displayStyle);
  prompt();

  console.log('\n=========================\n');

  console.log('| Równoważenie drzewa \'losowego\':');
  tree.displayTree(tree2,settings.displayStyle);
  //tree.displayTree(tree2,-1,'debug-before: ');
  tree.balance(tree2);
  //tree.displayTree(tree2,-1,'debug-after: ');
  prompt();

  console.log('\n| Drzewo po zrównoważeniu:');
  tree.displayTree(tree2,settings.displayStyle);
  prompt();
}

function treeSix(){
  console.clear();
  showData();
  console.log('_____________________________________\n');
  console.log('| Drzewo AVL:');
  tree.displayTree(tree1,settings.displayStyle);
  console.log('_____________________________________\n');
  console.log('| Drzewo \'losowe\':');
  tree.displayTree(tree2,settings.displayStyle);
  prompt();
}

function treeSeven(){
  console.clear();
  showData();
  console.log('_____________________________________\n');
  console.log('| Drzewo AVL:');
  tree.displayNodeBalance(tree1);
  console.log('\n| Drzewo \'losowe\':');
  tree.displayNodeBalance(tree2);
  prompt();
}
/////////          -- MENU --          /////////

//debug();

while(true){
  drawMainMenu(menuStyle);
  menuStyle = 0; //zakładamy, że nie zostanie popełniony błąd
  menuOption = prompt();
  //console.log(`Hey there ${reply}`);
  if (menuOption === '1'){
    optionOne();
    continue;
  }
  if (menuOption === '2'){
    optionTwo();
    continue;
  }
  if (menuOption === '3'){
    optionThree();
    continue;
  }
  if (menuOption === '4'){
    optionFour();
    continue;
  }
  if (menuOption === '5'){
    optionFive();
    continue;
  }
  if (menuOption === '6'){
    optionSix();
    continue;
  }
  if (menuOption === '0') {
    break;
  }
  menuStyle = 1; //popełniono błąd przy wprowadzaniu
}

function debug(){
  //funkcja uruchamiana przed wejściem do menu
  userData = [1,2,3,4,5];
  tree1 = tree.createAVL(userData);
  tree2 = tree.createRandom(userData);
}
