let treeEx = {};

//// -- FUNKCJE POMOCNICZE -- ////
function baseLog(a, b) {
    return Math.ceil(Math.log(a) / Math.log(b));
}
function comp (a,b){
    return a-b;
}
function getMaxLength(tree){
    let max = treeEx.getMax(tree).el;
    let min = treeEx.getMin(tree).el;
    let element;
    let lengthOfMin = (min < 0)? 1 : 0;
    
    if (min < 0) lengthOfMin++;
    element = Math.abs(min);
    while (element>0) {element=Math.floor(element/10); lengthOfMin++;}
    
    let lengthOfMax = (max < 0)? 1 : 0;
    element = Math.abs(max);
    while (element>0) {element=Math.floor(element/10); lengthOfMax++;}
    
    return Math.max(lengthOfMin, lengthOfMax);
}
function disp(number, width, style=0){
    //displays a number with a fixed width
    let length = (number<0)? 1 : 0;
    let e;
    if (number === undefined) {
      return ' '.repeat(width+2);
      number = '-'.repeat(width);
      length = width;
    }
    else if (number === 0){
      length = 1;
    }
    else {
      e = Math.abs(number);
      while (e>0) {e=Math.floor(e/10); length++;}
    }
    
    let leftSpace = Math.floor((width-length)/2);
    let rightSpace =  Math.ceil((width-length)/2)
    if (style === 1 || style === 3 || style === 5) return '<'.repeat(leftSpace+1) + number + '>'.repeat(rightSpace+1);
    else return ' '.repeat(leftSpace+1) + number + ' '.repeat(rightSpace+1);
  }















  //// -- FUNKCJE -- ////

treeEx.height = (tree, root=0) => {
  if (tree[root] === undefined) return 0;
  return 1 + Math.max(treeEx.height(tree, root*2 +1), treeEx.height(tree, root*2 +2));
};
treeEx.nodeBalance = (tree, nodeIndex) => {
  return treeEx.height(tree, nodeIndex*2 + 1) - treeEx.height(tree, nodeIndex*2 + 2);
};
treeEx.displayNodeBalance = (tree) => {
  tree.forEach((element) =>{
    if (element !== undefined)
      console.log(`Element ${element} ma współczynnik równowagi równy: ${treeEx.nodeBalance(tree, tree.indexOf(element))}`);
  })
}
treeEx.balance = (tree) => {
  let w = -1;
  for (let i=0;i<tree.length;++i){
    if (tree[i] === undefined) continue;
    if (Math.abs(treeEx.nodeBalance(tree, i)) > 1) {
      w = i;
      break;
    }
  }
  if (w === -1) return;
  let k = tree[w];
  let direction = 1; //1-lewo 2-prawo
  let leftHeight = treeEx.height(tree, w*2 + 1);
  let rightHeight = treeEx.height(tree, w*2 + 2);
  if (rightHeight > leftHeight) direction = 2;
  treeEx.removeElement(tree, k, direction);
  treeEx.addToTree(tree, k);
  treeEx.balance(tree);
};

 treeEx.displayTree = (tree, style = -1, caption = '') => {
    let undef = 0;
    let flagCount = 0;
    for(let i=0;i<tree.length;++i){
      if (tree[i] === undefined) {
        undef++; flagCount++;
      }
      else flagCount = 0;
    }
    for(let i=0;i<flagCount;++i){
      tree.pop(); undef--;
    }
     if (tree.length - undef > 10){
      console.log(`${caption}[... ${tree.length} elementów ...]`);
      return;
     }
     
     if (style === -1) {
       let treeString = '';
       let element;
       let puste = 0;
       let comma = '';
       for (let index=0;index<tree.length;++index){
         if (index>0) comma = ',';
         element = tree[index];
         if (element !== undefined){
           if (puste === 1) treeString += comma + '-';
           else if (puste !== 0){
             treeString += `${comma}(puste: ${puste})`
           }
           puste = 0;
           treeString += comma + element;
         }
         else { puste++; continue; }
       }
       //usuwanie pustych elementow z konca drzewa
       while (puste > 0) {
        tree.pop();
        puste--;
       }
       /*
       ALTERNATYWA: WYŚWIETLANIE PUSTYCH NA KOŃCU
       if (puste > 0) {
        if (puste === 1) treeString += '-';
        else if (puste !== 0){
          treeString+= `(puste: ${puste})`
        }
       }
       */
       console.log(`${caption}[${treeString}]`);
       return;
     }
     let x = 0;
     let height = treeEx.height(tree);
     let maxLength = getMaxLength(tree);
     let maxWidth = Math.pow(2, height - 1) * maxLength + Math.pow(2, height - 1) - 1;
     let space;
     for (let h = 0; h < height; ++h) {
         let line = '';
         let elements = Math.pow(2, h);
         let w = elements + x;

         space = Math.floor((maxWidth - (elements) * maxLength) / (elements));
         let first = true;
         while (x < w) {
             //if (!first) {
             for (let i = 0; i < space; ++i)
                 line += ' ';
             //}
             first = false;
             line += disp(tree[x++], maxLength, style);
             if (x >= w)
                 break;
             for (let i = 0; i < space + maxLength; ++i)
                 line += ' ';
             //++x;
         }
         if (style > 1) {
             for (let powt = 0; powt < space / 2; ++powt) {
                 line += '\n';
                 x -= elements;
                 first = true;
                 while (x < w) {
                     //if (!first) {
                     for (let i = 0; i < space - powt; ++i)
                         line += ' ';
                     //}
                     first = false;
                     if (tree[Math.floor(2 * x + 1)] !== undefined) {
                         line += '/';
                     }
                     else
                         line += ' ';
                     line += ' '.repeat(maxLength + powt * 2);
                     if (tree[Math.floor(2 * x + 2)] !== undefined)
                         line += '\\';
                     else
                         line += ' ';
                     ++x;
                     if (x >= w)
                         break;
                     for (let i = 0; i < space + maxLength - powt; ++i)
                         line += ' ';
                     //++x;
                 }
                 if (style < 4)
                     break;
             }
         }
         console.log(line);
     }
 }
treeEx.addToTree = (tree, element) => {
    let whereTo = 0;
    while (tree[whereTo] !== undefined) {
        if (element > tree[whereTo])
            whereTo = whereTo * 2 + 2;
        else
            whereTo = whereTo * 2 + 1;
    }
    //console.log(`inserting ${element} to ${tree} at index ${whereTo}`)
    tree[whereTo] = element;
}

treeEx.getMin = (tree, root=0) => {
    // gets the minimum element from a tree
    // (and the path that lead to it)
    let whereTo = root;
    let path = [tree[root]];
    while (tree[whereTo*2 + 1] !== undefined){
      path.push(tree[whereTo*2 + 1]);
      whereTo = whereTo*2 + 1;
    }
    //path.pop();
    return {el: tree[whereTo], path: path};
  }

treeEx.getMax = (tree, root=0) => {
    // gets the maximum element from a tree
    // (and the path that lead to it)
    let whereTo = root;
    let path = [tree[root]];
    while (tree[whereTo*2 + 2] !== undefined){
      path.push(tree[whereTo*2 + 2]);
      whereTo = whereTo*2 + 2;
    }
    //path.pop();
    return {el: tree[whereTo], path: path};
  }
  
let output = [];
treeEx.bst = (arr) => {
    let l = arr.length;
    if (l<2) {
      treeEx.addToTree(output, arr[0]);
      return;
    }
    let medianIx = Math.floor((l-1)/2);
    let median = arr[medianIx];
    
    treeEx.addToTree(output, median);
    
    let left = arr.slice(0,medianIx);
    let right = arr.slice(medianIx+1);
  
    treeEx.bst(left);
    treeEx.bst(right);
  }

treeEx.createAVL = (arr, needToSort=true) => {
  let arrCopy = [].concat(arr);
  if (needToSort) arrCopy.sort(comp);
  output = [];
  treeEx.bst(arrCopy);
  return output;
}
treeEx.createRandom = (arr) => {
  let arrCopy = [].concat(arr);
  let outTree = [];
  while(arrCopy.length){
    treeEx.addToTree(outTree,arrCopy.shift());
  }
  return outTree;
}
treeEx.listSubtree = [
  (tree, root) => { 
    output.push(tree[root]);
    if (tree[root*2+1] !== undefined) treeEx.listSubtree[0](tree, root*2+1);
    if (tree[root*2+2] !== undefined) treeEx.listSubtree[0](tree, root*2+2);
  },
  (tree, root) => {
    if (tree[root*2+1] !== undefined) treeEx.listSubtree[1](tree, root*2+1);
    output.push(tree[root]);
    if (tree[root*2+2] !== undefined) treeEx.listSubtree[1](tree, root*2+2);
  },
  (tree, root) => {
    if (tree[root*2+1] !== undefined) treeEx.listSubtree[2](tree, root*2+1);
    if (tree[root*2+2] !== undefined) treeEx.listSubtree[2](tree, root*2+2);
    output.push(tree[root]);
  }
];
treeEx.listTree = (tree, method, key='root') => {
  output = [];
  let root = (key === 'root')? 0 : tree.indexOf(key);
  treeEx.listSubtree[method](tree, root);
  return output;
};
treeEx.bringUpLeft = (tree, son, whereFather) => {
// przykleja z powrotem poddrzewo oderwane poprzez usunięcie ojca, który
// miał syna tylko po lewej stronie
// (prawe dzieci idą na poziom ojca i na prawo, a lewe dzieci po prostu zastepuja ojca)
//whereSon mówi gdzie syn ma trafić: 0- miejsce ojca, 1- na prawo od ojca
tree[Math.floor((son-1)/2) + whereFather] = tree[son]; //syn zastepuje ojca
tree[son] = undefined; //i zostaje usuniety ze starej pozycji
if (tree[son*2+1] !== undefined) treeEx.bringUpLeft(tree, son*2+1, 0);
if (tree[son*2+2] !== undefined) treeEx.bringUpLeft(tree, son*2+2, 1);
};
treeEx.bringUpRight = (tree, son, whereFather) => {
  tree[Math.floor((son-1)/2) + whereFather] = tree[son]; //syn zastepuje ojca
  tree[son] = undefined; //i zostaje usuniety ze starej pozycji
  if (tree[son*2+1] !== undefined) treeEx.bringUpRight(tree, son*2+1, -1);
  if (tree[son*2+2] !== undefined) treeEx.bringUpRight(tree, son*2+2, 0);
};

treeEx.removeElement = (tree, element, preferredDirection=0) => {
  let index = tree.indexOf(element);
  let lewy = 0;
  let prawy = 0;
  if (tree[index*2+1] !== undefined) lewy++;
  if (tree[index*2+2] !== undefined) prawy++;
  if (lewy+prawy === 0) {
    tree[index] = undefined;
    return;
  }
  if (lewy+prawy === 2) {
    // usuwanie elementu z dwoma węzłami potomnymi
    let lewySasiad = treeEx.getMax(tree, index*2+1).el;
    let prawySasiad = treeEx.getMin(tree, index*2+2).el;
    let sasiad = lewySasiad;
    if (Math.abs(element-prawySasiad) < Math.abs(element-lewySasiad)){
      sasiad = prawySasiad;
    }
    if (preferredDirection === 0){
      treeEx.removeElement(tree, sasiad);
      tree[index] = sasiad;
      return;
    }
    if (preferredDirection === 1) sasiad = lewySasiad;
    else sasiad = prawySasiad;
    treeEx.removeElement(tree, sasiad);
    tree[index] = sasiad;
    return;
  }
  if(lewy){
  // jest tylko lewe poddrzewo
    treeEx.bringUpLeft(tree, index*2+1, 0);
    return;
  }
  // jest tylko prawe poddrzewo
    treeEx.bringUpRight(tree, index*2+2, 0);
};




  //// -- FUNKCJE DO DANYCH -- ////
treeEx.genData = (numberOfElements, max, min=10) => {
    let arr = [];
    for(let i=0;i<numberOfElements;++i){
      let n;
      do
        n = Math.floor(Math.random() * (max - min + 1) + min);
      while(arr.indexOf(n) !== -1)
      arr.push(n);
    }
    return arr;
  }











  // NIEUŻYWANE //
  /*
  function getParent(output, value){
    index = output.indexOf(value);
    parent = output[Math.floor((index-1)/2)];
    return parent;
  }
  function getLeftChild(output, value){
    index = output.indexOf(value);
    left_child = output[2*index+1];
    return left_child;
  }
  function getRightChild(output, value){
    index = output.indexOf(value);
    right_child = output[2*index+2];
    return right_child;
  }
*/
module.exports = treeEx;